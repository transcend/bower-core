# Core Module
The "Core" module is a small library of simple and basic functionality that is commonly used in many applications. The
goal of this module is to provide a small base set of code to build off of. The majority of components in this module
are utilitarian in nature. Many other modules have a dependency on this module. In addition, any common interfaces are
included in this module.[More details here](http://transcend.bitbucket.org/#/api/transcend.core).

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-core.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-core/get/master.zip](http://code.tsstools.com/bower-core/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-core](http://code.tsstools.com/bower-core)
* If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/core/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.core']);
```

## To Do
- Determine if the $array, $string, and $object services should be moved into a separate "utils" module.
- Implement something similar to $array in the $color factory.

## Project Goals
- Keep this module size below 6kb minified