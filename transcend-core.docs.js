/**
 * @ngdoc overview
 * @name transcend.core
 * @description
 # Core Module
 The "Core" module is a small library of simple and basic functionality that is commonly used in many applications. The
 goal of this module is to provide a small base set of code to build off of. The majority of components in this module
 are utilitarian in nature. Many other modules have a dependency on this module. In addition, any common interfaces
 are included in this module.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-core.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-core/get/master.zip](http://code.tsstools.com/bower-core/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-core](http://code.tsstools.com/bower-core)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/core/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.core']);
 ```

 ## To Do
 - Determine if the {@link transcend.core.$array $array}, {@link transcend.core.$string $string}, and
 {@link transcend.core.$object $object} services should be moved into a separate "utils" module.
 - Implement something similar to $array in the $color factory.
 - Deprecate the 'tsConfig' object once the other modules have been weened off of it.

 ## Project Goals
 - Keep this module size below 10kb minified
 */
/**
 * @ngdoc service
 * @name transcend.core.$array
 *
 * @description
 * Provides a set of 'array' utility functions. These methods are 'static' functions found on the
 * factory object. In addition, you can call the factory function to retrieve an instantiated 'array'
 * utility instance. This instance has all of the same methods that the static 'factory' object has,
 * except you do not have to pass in the 'array' reference to the function. The point of the instantiable
 * 'array' utility is to cache a particular item and execute multiple calls on that item without having
 * to pass the item reference in each time.
 *
 <h3>Calling Static Array Utility Functions</h3>
 <pre>
 $array.someMethod(arrayReference, args);
 </pre>

 <h3>Instantiating an Array Utility</h3>
 <pre>
 // By instantiating an "Array" utility we cache the array reference in the
 // instance and therefore do not pass that array reference to any of the calls.
 var util = $array(arrayReference);
 util.someMethod(args);
 util.someOtherMethod(otherArgs);
 </pre>
 *
 * @param {Array=} cachedItem The array that you want to 'cache' to perform operations against.
 */
/**
     * Compares values and returns the best value that passes the comparison function.
     * @param {Array} source The source array to check each item in.
     * @param {Function} comparisonFn The function to use to compare items.
     * @returns {*} The value that matched the compare function.
     * @private
     */
/**
     * Returns a function that will serve as a comparer to sort by a property.
     * @param {String} property The property to sort on. If prefixed with a negative it will sort descending.
     * @returns {Function} The comparer function.
     * @private
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#isArray
     * @methodOf transcend.core.$array
     *
     * @description
     * Determines if a reference is an 'Array'.
     *
     * @param {*} value Reference to check.
     * @returns {Boolean} True if 'value' is an 'Array'.
     * @example
     <pre>
     expect($array.isArray([])).toBeTruthy();
     expect($array.isArray({})).toBeFalsy();
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#toMap
     * @methodOf transcend.core.$array
     *
     * @description
     * Converts an array to an object (key/value pair).
     *
     * @param {Array} src The source array to build a dictionary from.
     * @param {String} keyProperty The element property value to be used as the dictionary key.
     * @param {*=} value If provided, sets the dictionary item value to the specified property value, otherwise the
     * item value will be the input array's element
     * @returns {Object} Returns an object with each input array element as a property value and each property named
     * with the specified keyProp property's value.
     * @example
     <pre>
     // Map an array of objects by the 'z' property.
     var input = [{x: "value1a", y: "value1b", z: "value1c"}, {x: "value2a", y: "value2b", z: "value2c"}, {x: "value3a", y: "value3b", z: "value3c"}];

     // The array will be turned into a dictionary based on the 'z' property.
     expect($array.toMap(input)).toEqual({
         value1c: {x: "value1a", y: "value1b", z: "value1c"},
         value2c: {x: "value2a", y: "value2b", z: "value2c"},
         value3c: {x: "value3a", y: "value3b", z: "value3c"}
       });
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#fromMap
     * @methodOf transcend.core.$array
     *
     * @description
     * Converts an object into a simple key/value pair dictionary array.
     *
     * @param {Object} src The source object to use to create the key/value array from.
     * @param {String=} keyField The field to use for the key property.
     * @param {String=} valueField The field to use for the value property.
     * @returns {Array} An array of key/value pairs - based on the key/value field passed in.
     * @example
     <pre>
     var map = {e: 'e', f: false, g: null};
     expect($array.fromMap(map)).toEqual([
     {key: 'e', value: 'e'},{key: 'f', value: false},{key: 'g', value: null}
     ]);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#unique
     * @methodOf transcend.core.$array
     *
     * @description
     * Creates a new array with unique values from the source array. The unique values can be configured
     * to look at a specific property and will accept nested properties within an object (ex: 'x.y.z').
     *
     * @param {Array} items The array of items to be filtered.
     * @param {String=} filterOn The attributes, if applicable, to filter the array by.
     * @returns {Array|undefined} Returns the array of filtered items based off the given attribute.
     * @example
     <h3>Filtering on Primitive Types</h3>
     <pre>
     expect($array.unique(['one', 'one', 'two'])).toEqual(['one', 'two']);
     </pre>
     <h3>Filtering on Single-Level Objects</h3>
     <pre>
     // Will filter on a single-level object.
     expect($array.unique([{x:1}, {x:1}, {x:2}])).toEqual([{x:1}, {x:2}]);
     </pre>
     <h3>Filtering on Multi-Level Objects</h3>
     <pre>
     expect($array.unique([
     {attributes:{x:1, y:2}},
     {attributes:{x:1, y:2}},
     {attributes:{x:2, y:2}}
     ], 'attributes.x')
     .toEqual([
     {attributes:{x:1, y:2}},
     {attributes:{x:2, y:2}}]
     );
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#min
     * @methodOf transcend.core.$array
     *
     * @description
     * Returns the minimum value in an array.
     *
     * @param {Array} source The Array from which to select the minimum value.
     * @return {Number} The minimum value in the source array.
     * @example
     <pre>
     expect($array.min([1, 2, 3, 4, 5, 6, 7, -5, 20])).toEqual(-5);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#max
     * @methodOf transcend.core.$array
     *
     * @description
     * Returns the maximum value in the Array.
     *
     * @param {Array} source The Array from which to select the maximum value.
     * @return {Number} The maximum value in the source array.
     * @example
     <pre>
     expect($array.max([1, 2, 3, 4, 20, 5, 6, 7, -5])).toEqual(20);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#sortByProperty
     * @methodOf transcend.core.$array
     *
     * @description
     * Used as a comparison function to sort an array of objects on a specified property. Sorting can be set to be
     * ascending or descending. Sorting default is ascending. To sort descending add a minus prefix to the passed
     * property name string, e.g '-propertyName'.
     *
     * @param {Array} source The array to sort on.
     * @param {String} property The property to sort on. Prefix the property with a negative to sort descending. The
     * default value for this property is 'order'.
     * @returns {transcend.core.$array} The instance of $array (for chaining).
     * @example
     <h3>Sort by Default 'order' Property</h3>
     <pre>
     source = [{order: 3}, {order: 1}, {order: 5}];
     $array.sortByProperty(source);
     expect(source).toEqual([{order: 1}, {order: 3}, {order: 5}]);
     </pre>
     <h3>Sort Ascending by 'x' Property</h3>
     <pre>
     var source = [{x: 3}, {x: 1}, {x: 5}];
     $array.sortByProperty(source, 'x');
     expect(source).toEqual([{x: 1}, {x: 3}, {x: 5}]);
     </pre>
     <h3>Sort Descending by 'x' Property</h3>
     <pre>
     var source = [{x: 3}, {x: 1}, {x: 5}];
     $array.sortByProperty(source, '-x'); // Prefix with '-' for descending.
     expect(source).toEqual([{x: 1}, {x: 3}, {x: 5}]);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#count
     * @methodOf transcend.core.$array
     *
     * @description
     * Returns the length of an array given a filter - the length after applying the filter.
     *
     * @param {Array} source The source array to get the count on.
     * @param {Function} filter The filter function to limit the array by.
     * @returns {Number} The array length after the filter is applied.
     * @example
     <pre>
     expect($array.count([1, 2, 3], function (item) {
         return item > 1;
       })).toEqual(2);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#containsAll
     * @methodOf transcend.core.$array
     *
     * @description
     * Determines if an array contains all of the items passed in as arguments.
     *
     * @param {Array} source The source array to check against.
     * @param {Array|...*} items The items to check if they are present in the source array.
     * @returns {Boolean} If the source array contains all of the items passed in.
     * @example
     <pre>
     expect($array.containsAll([1, 2, 3], 1, 2, 3)).toBeTruthy();
     expect($array.containsAll([1, 2, 3], [1, 2, 3])).toBeTruthy();
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#containsAny
     * @methodOf transcend.core.$array
     *
     * @description
     * Determines if an array contains any of the items passed in as arguments.
     *
     * @param {Array} source The source array to check against.
     * @param {Array|...*} items The items to check if they are present in the source array.
     * @returns {Boolean} If the source array contains any of the items passed in.
     * @example
     <pre>
     expect($array.containsAll([1, 2, 3], 8, 3, 9)).toBeTruthy();
     expect($array.containsAll([1, 2, 3], [8, 3, 9])).toBeTruthy();
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#pluck
     * @methodOf transcend.core.$array
     *
     * @description
     * Plucks the value of a property from each item in the Array.
     *
     * @param {Array} array The Array of items to pluck the value from.
     * @param {String} propertyName The property name to pluck from each element.
     * @returns {Array} The value from each item in the Array.
     * @example
     <pre>
     // Note that the 3rd item in the array does not have an id property and is therefore skipped.
     expect($array.pluck([{id: 1}, {id: 2}, {name: 5}, {id: 3}], 'id')).toBeEqual([1,2,3]);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#findById
     * @methodOf transcend.core.$array
     *
     * @description
     * Retrieves an item in an array based on the value of a specified property. The default property is 'id'.
     *
     * @param {Array} list The array to search for the item in.
     * @param {*} value The value of the property to search on.
     * @param {String} field The field to search on. The default value is 'id'.
     * @returns {*|undefined} The found object or undefined if no object found.
     * @example
     <h3>Finding by the Default 'id' Property</h3>
     <pre>
     expect($array.findById([ {id: 1}, {id: 2}, {id: 3}], 2)).toEqual({id: 2});
     </pre>
     <h3>Finding by a Specified Property</h3>
     <pre>
     var item = $array.findById([{foo: 9}, {foo: 8}, {foo: 7}], 7, 'foo');
     expect(item).toEqual({foo: 7});
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$array#merge
     * @methodOf transcend.core.$array
     *
     * @description
     * Merges objects of two arrays. If the values are primitive it will concatenate them onto the
     * destination array. If they are object's it will merge the two object's properties.
     *
     * @param {Array} destination The array to merge into.
     * @param {Array} source The array to merge from.
     * @param {String} mergeArraysPropertyIdentifier The identifier to determine if an object in the source array is
     * the same as an object in the destination array. The default value of is 'id'.
     *
     * @example
     <h3>Merging Primitive Values</h3>
     <pre>
     var destination = [2, 4, 6, 8];
     $array.merge(destination, [1, 3, 5, 7]);
     expect(destination).toEqual([2, 4, 6, 8, 1, 3, 5, 7]);
     </pre>

     <h3>Merging Objects by Default 'id' Property</h3>
     <pre>
     var destination = [{id: 1},{id: 2, x: 9, y: 7}];
     $array.merge(destination, [{id: 2, x: 10, z: 2},{id: 3}]);

     expect(destination).toEqual([{id: 1},{id: 2, x: 10, y: 7, z: 2},{id: 3}]);
     </pre>

     <h3>Merging Objects by Specified Property</h3>
     <pre>
     var destination = [{name: 'one'},{x: 9, y: 7, name: 'two'}];
     $array.merge(destination, [{x: 10, z: 2, name: 'two'},{name: 'three'}], 'name');

     expect(destination).toEqual([
     {name:'one'},
     {x:10, y:7, z:2, name:'two'},
     {name:'three'}
     ]);
     </pre>
     */
/**
 * @ngdoc service
 * @name transcend.core.$errorMsg
 *
 * @description
 * Provides a simple method to extract/find an error message from an error/exception object.
 *
 <pre>
 // String message:
 expect($errorMsg('oh no!')).toEqual('oh no!');

 // Single level deep message:
 expect($errorMsg({message: 'msg'})).toEqual('msg');
 expect($errorMsg({exceptionMessage: 'msg'})).toEqual('msg');
 expect($errorMsg({msg: 'msg'})).toEqual('msg');
 expect($errorMsg({error: 'msg'})).toEqual('msg');
 expect($errorMsg({data: 'msg'})).toEqual('msg');
 expect($errorMsg({exception: 'msg'})).toEqual('msg');

 // Nested message:
 expect($errorMsg({data: {
  exception: {
    exceptionMessage: 'msg'
  }
 }})).toEqual('msg');
 </pre>
 *
 * @param {string|object} error The error to find the message in.
 */
/**
 * @ngdoc service
 * @name transcend.core.$loading
 *
 * @description
 * Provides a set of utility functions for use while the application is loading data. These methods are 'static'
 * functions found on the factory object.
 *
 <h3>Calling Static Loading Utility Functions</h3>
 <pre>
 $loading.someMethod(args);
 </pre>
 */
/**
     * @description
     * Allows the progress, message and securityKey values to be set from code
     *
     * @param {int} progress The percent of completion of the loading operation.
     * @param {String} message The message to display to the user.
     * @param {String} securityKey The securityKey, if any, used to determine lockedDown status
     * @returns {String} Returns the resulting string value
     * @example
     <pre>
     // Convert values to a string
     expect($csv.stringify('def')).toEqual('def');
     expect($csv.stringify2(true)).toEqual('TRUE');
     expect($csv.stringify2(23)).toEqual('23');
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$loading.lockedDown
     * @methodOf transcend.core.$loading
     *
     * @description
     * If a securityKey is saved for this session, and if a non-null securityKey is passed in, and that key does not
     * equal the saved securityKey, then true is returned.
     * If a securityKey is saved for this session, and if a non-null securityKey is passed in, and that key does equal
     * the saved securityKey, then false is returned, indicating that the operation is progress can proceed because it
     * is controlled by the user that locked the loading process.
     * If no securityKey is saved for this session, and if a non-null securityKey is passed in, the securityKey that
     * was passed in is saved and false is returned.
     * if no securityKey is saved for this session, and if no securityKey is passed in, false is returned.
     *
     * @param {String} securityKey The securityKey, if any, used to determine whether or not the loading
     *   process is locked down
     * @returns {Boolean} Returns true or false based on the rules in the description above
     * @example
     <pre>
     // Check lockedDown status of the loading process
     expect($loading.lockedDown('currentSecurityKey')).toEqual(false);
     expect($loading.lockedDown('notCurrentSecurityKey')).toEqual(true);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$loading.idle
     * @methodOf transcend.core.$loading
     *
     * @description
     * Set the progress to the current progress value, and the message to the one passed in, if any, for the
     * currently running loading process.
     *
     * @param {String} message The message to use for updating the loading process
     * @example
     <pre>
     // Update a current loading process.
     $loading.idle('message1') -- this should fail if there is a saved security key; if not, it should succeed
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$loading.startAt
     * @methodOf transcend.core.$loading
     *
     * @description
     * If the loading process is not locked down, set a specified progress point message for the currently running
     * loading process.
     *
     * @param {Number} progress The progress percentage to use for updating the loading process
     * @param {String} message The message to display in the loading process
     * @param {String} securityKey The securityKey, if any, used to determine whether or not the loading
     *   process is locked down
     * @example
     <pre>
     // Update a currently running loading process, if not locked down.  Assume a valid current security key is saved.
     $loading.startAt(50, 'new message1', 'currentSecurityKey') -- this should update the loading process
     $loading.startAt(25, 'new message1', 'notCurrentSecurityKey') -- this should fail to update the loading process
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$loading.start
     * @methodOf transcend.core.$loading
     *
     * @description
     * If the loading process is not locked down, start a new loading process, resetting the message, progress and
     * total number of ticks values.  This method calls the increment method.
     *
     * @param {Number} totalTicks The maximum number of ticks allowed for this loading process
     * @param {String} message The message to display when the loading process is running
     * @param {String} securityKey The securityKey, if any, used to determine whether or not the loading process is locked down
     * @example
     <pre>
     // Start a new loading process, if not locked down.  Assume a valid current security key is saved.
     $loading.start(5, 'message1', 'currentSecurityKey') -- this should start a new loading process
     $loading.start(5, 'message1', 'notCurrentSecurityKey') -- this should fail to start a new loading process
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$loading.increment
     * @methodOf transcend.core.$loading
     *
     * @description
     * If the loading process is not locked down, update the ticks, message and progress values for the currently
     * running loading process.  If updating the ticks causes that value to be greater than the total number of
     * ticks, then call the stop method.
     *
     * @param {String} message The message to display when the loading process is running
     * @param {String} securityKey The securityKey, if any, used to determine whether or not the loading process is locked down
     * @example
     <pre>
     // Increment the values in the loading process.  Assume a valid current security key is saved.
     $loading.increment('message1', 'currentSecurityKey') -- this should update the loading process
     $loading.increment('message1', 'notCurrentSecurityKey') -- this should fail to update the loading process
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$loading.stop
     * @methodOf transcend.core.$loading
     *
     * @description
     * Stops the loading display, if the loading process is not locked down; otherwise, has no effect.
     *
     * @param {String} securityKey The securityKey, if any, used to determine whether or not the loading process is locked down
     * @example
     <pre>
     // Stop the loading display.  Assume a valid current security key is saved.
     $loading.stop('currentSecurityKey') -- this should stop the current loading process
     $loading.stop() -- this should fail to stop the current loading process
     </pre>
     */
/**
 * @ngdoc service
 * @name transcend.core.$notify
 *
 * @description
 * the "$notify" factory provides functionality for the notify system to help log errors and other important things
 * as they occur within the app. The factory provides the ability to log messages, track a promise, show waiting
 * messages, and provide success/error feedback. The $notify factory is typically extended using a "toaster" notify
 * decorator to add behaviour.
 *
 * The concept of the "$notify" factory is to provide a single common interface that components can use to communicate
 * information to the user, the individual application can than determine how to display that information to the user.
 *
 <pre>
 $scope.working = true;

 $notify(Account.changePassword(null, model))
 .wait('Saving password change. Please wait.', 'Saving Password')
   .success('Password changed successfully', 'Password Changed')
   .error('Failed to change password. {0}', 'Password Change Failed')
   .log('Password changed', 'Failed to change password')
   .promise.finally(function () {
        $scope.working = false;
      });
 </pre>
 *
 * @param {object=} promise A promise or object that has a $promise property, to track.
 * @param {object=} log A log item to track, and update, with the promise.
 *
 * @requires $log
 * @requires transcend.core.$string
 * @requires transcend.core.$errorMsg
 * @requires transcend.core.tsConfig
 */
/**
     * @ngdoc method
     * @name transcend.core.$notify#wait
     * @methodOf transcend.core.$notify
     *
     * @description
     * Indicates a state of "waiting". Typically will show a spinner in this state.
     *
     * @param {String=} msg The wait message. Will default to a generic message.
     * @param {String=} title The wait message title. Will default to a generic title.
     * @param {Number=} waitTime The maximum amount of time (seconds) to show the "wait" state. Will default to "infinite" time.
     * @returns {transcend.core.$notify} A $notify instance.
     *
     * @example
     <pre>
     $notify(Account.changePassword(null, model))
       .wait('Saving password change. Please wait.', 'Saving Password');
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$notify#success
     * @methodOf transcend.core.$notify
     *
     * @description
     * Provides a notification on success (when the promise is resolved).
     *
     * @param {String=} msg The message to show. Will default to a generic message.
     * @param {String=} title The title to show. Will default to a generic title.
     * @returns {transcend.core.$notify} A $notify instance.
     *
     * @example
     <pre>
     $notify(Account.changePassword(null, model))
       .success('Saved password. Congrats!', 'Password Saved');
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$notify#error
     * @methodOf transcend.core.$notify
     *
     * @description
     * Provides a notification on error (when the promise is rejected).
     *
     * @param {String=} msg The message to show when an error occurs. Will default to a generic message. Any instances
     * of {0} will be replaced with the inner most exception message (if an exception is returned via the promise
     * rejection.
     * @param {String=} title The title to show. Will default to a generic title.
     * @returns {transcend.core.$notify} A $notify instance.
     *
     * @example
     <pre>
     $notify(Account.changePassword(null, model))
       .error('Failed to save password. Ooops! {0}', 'Save Failed');
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$notify#log
     * @methodOf transcend.core.$notify
     *
     * @description
     * Provides a method to have the $notify instance automatically log messages, based on the state. If the promise is
     * resolved it will log the "msg", otherwise it the promise is failed it will log the "errorMsg".
     *
     * @param {String=} msg The message to be logged on success (promise is resolved).
     * @param {String=} errorMsg The message to be logged on error (promise is rejected). Any instances of {0} will be
     * replaced with the inner most exception message (if an exception is returned via the promise rejection.
     * @param {String=} skipTimeTrack Flag to determine whether to use the $log's start/stop method (stopwatch tracking).
     * @param {String=} logClosed Flag to determine whether to catch any other logs and add them as children to this log.
     * @returns {transcend.core.$notify} A $notify instance.
     *
     * @example
     <pre>
     $notify(Account.changePassword(null, model))
       .log('Saved profile', 'Failed to save profile. {0}');

     $notify(Account.changePassword(null, model))
       .log('Saved profile', 'Failed to save profile. {0}', false, true);
     </pre>
     */
/**
 * @ngdoc service
 * @name transcend.core.$object
 *
 * @description
 * Provides a set of 'object' utility functions. These methods are 'static' functions found on the
 * factory object. In addition, you can call the factory function to retrieve an instantiated 'object'
 * utility instance. This instance has all of the same methods that the static 'factory' object has,
 * except you do not have to pass in the 'object' reference to the function. The point of the instantiable
 * 'object' utility is to cache a particular item and execute multiple calls on that item without having
 * to pass the item reference in each time.
 *
 * @requires $protoFactory
 <pre>
 // Static method call.
 $object.someMethod(objectReference, args);

 // Instantiable method calls. Note, the string reference is not passed in as the first argument anymore.
 var obj = $object(objectReference);
 obj.someMethod(args);
 obj.someOtherMethod(otherArgs);
 </pre>
 *
 * @param {Object=} cachedItem The object that you want to 'cache' to perform operations against.
 */
/**
     * Retrieves an item in an array based on the value of a specified property.
     * @param list
     * @param value
     * @param field
     * @returns {*}
     * @private
     */
/**
     * Merges objects of two arrays. If the values are primitive it will concatenate them onto the
     * destination array. If they are object's it will merge the two object's properties.
     * @param {Array} destination
     * @param {Array} source
     * @param mergeArraysPropertyIdentifier
     * @private
     */
/**
     * @ngdoc method
     * @name transcend.core.$object#extend
     * @methodOf transcend.core.$object
     *
     * @description
     * Extends the destination object 'dst' by copying all of the properties from the 'src' object(s)
     * to 'dst'. You can specify multiple 'src' objects.
     *
     * @param {Object} dst Destination object.
     * @param {...Object} src Source object(s).
     * @returns {Object} Reference to 'dst'.
     *
     * @example
     <h4>Static Method</h4>
     <pre>
     var source1 = {a: 'a', b: 'b'};
     var source2 = {c: 'c'};
     var destination = {d: 'd'};

     // The 'destination' property will be extended.
     $object.extend(destination, source1, source2);

     expect(destination).toEqual({ a: 'a', b: 'b', c: 'c', d: 'd' });
     </pre>

     <h4>Instance Method</h4>
     <pre>
     var destination = {d: 'd'};
     var obj = $object(destination);

     // Note, the 'destination' object is not passed in.
     obj.extend({a: 'a', b: 'b'}, {c: 'c'});

     expect(destination).toEqual({ a: 'a', b: 'b', c: 'c', d: 'd' });
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$object#areNumbers
     * @methodOf transcend.core.$object
     *
     * @description
     * Determines if all of the passed in arguments are numbers.
     * @param {Object...} tests Objects to test whether they are numbers or not.
     * @returns {Boolean} Returns true if all of the arguments are numbers.
     *
     * @example
     <h4>Static Method</h4>
     <pre>
     expect($object.areNumbers(-1, 0, 1, 2, 3, 4, 5)).toBeTruthy();
     expect($object.areNumbers(1, 2, 3, 'four')).toBeFalsy();
     </pre>

     <h4>Instance Method</h4>
     <pre>
     var obj = $object('nine');

     // This fails because the cached object 'nine' will be passed in as the first argument.
     expect(obj.areNumbers(5)).toBeFalsy();
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$object#traverse
     * @methodOf transcend.core.$object
     *
     * @description
     * Traverses an object based on a string representation/configuration of attributes.
     *
     * @param {Object} object Object to traverse.
     * @param {String} map Map (representation/configuration) of the attributes to traverse to.
     * @returns {Object} The object property that was traversed to.
     * @example
     <h4>Static Method</h4>
     <pre>
     expect($object.traverse({x: 9}, 'x')).toEqual(9);
     expect($object.traverse({x: { y: 5 }}, 'x.y')).toEqual(5);
     </pre>

     <h4>Instance Method</h4>
     <pre>
     var obj = $object({x: { y: 5 }});
     expect(obj.traverse('x').toEqual({ y: 5 });
     expect(obj.traverse('x.y').toEqual(5);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$object#key
     * @methodOf transcend.core.$object
     *
     * @description
     * Returns the first (if any) key that the object has. The main point of this function is to
     * be able to pass in a number of keys that may match, and the function will return the first
     * key that matches. An example scenario of this would be if you wanted to determine what the
     * 'longitude' property of a 'center' object but you're not sure what property was actually
     * used - like: lon, x, longitude, etc.
     *
     * @param {Object} object The object to check for properties in.
     * @param {String|Array} attributes Potential attributes to check if the object has.
     * @returns {String|undefined} The first attribute that the object has - if any. Undefined will be returned
     * if no attribute is found.
     * @example
     <h4>Static Method</h4>
     <pre>
     expect($object.key({a: 9, x: 5, lon: 8}, 'lon, x')).toEqual('lon');
     </pre>

     <h4>Instance Method</h4>
     <pre>
     var obj = $object({a: 9, x: 5, lon: 8});

     expect(obj.key('lon, x')).toEqual('lon');
     expect(obj.key(['x', 'lon'])).toEqual('x');
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$object#value
     * @methodOf transcend.core.$object
     *
     * @description
     * Gets or sets the value of a given attribute in an object. The attribute 'definition' can be a multi-level string
     * representation of the attribute - like 'x.y.z'.
     *
     * @param {Object} object The object to get/set the value to/from.
     * @param {String} attributes Attribute(s) to get/set the value to/from.
     * @param {*} setterValue A value to set the object's attribute to. Note, if a attribute
     * configuration of 'x.y.z' is passed in, than 'z' will be set to the value of 'setValue'.
     * @returns {*} The value of the attribute. If a 'setterValue' was passed than
     * the 'setterValue' will be returned if the value was actually set.
     * @example
     <h4>Static Method</h4>
     <pre>
     expect($object.value({a: 9, x: 5, lon: 8}, 'lon, x')).toEqual(8);
     </pre>

     <h4>Instance Method</h4>
     <pre>
     var obj = $object({a: 9, x: 5, lon: 8});

     expect(obj.value('lon, x')).toEqual(8);
     expect(obj.value(['x', 'lon'])).toEqual(5);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$object#hasKeys
     * @methodOf transcend.core.$object
     *
     * @description
     * Determines if an object has all of a the passed in attributes - 'keys'.
     *
     * @param {Object} object The object to test if it has all of the attributes against.
     * @param {String...} attributes Attribute(s) to test if the object has.
     * @returns {Boolean} Returns 'true' if the object has all of the keys passed in or.
     * @example
     <h4>Static Method</h4>
     <pre>
     expect($object.hasKeys({a: 9, x: 5, lon: 8}, 'lon, x')).toBeTruthy();
     </pre>

     <h4>Instance Method</h4>
     <pre>
     var obj = $object({a: 9, x: 5, lon: 8});

     expect(obj.hasKeys('lon, x')).toBeTruthy();
     expect(obj.hasKeys(['x', 'lon', 'blah'])).toBeFalsy();
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$object#merge
     * @methodOf transcend.core.$object
     *
     * @description
     * Merges two objects. This method is similar to extend however instead of overwriting
     * objects it will traverse them and add to the properties and/or only overwrite simple types.
     *
     * @param {Object} destination The object to add properties/values to (merge into).
     * @param {Object} source Object to merge into the destination object.
     * @param {Object} opts Merge options.
     * @returns {Object} Reference to 'destination'.
     * @example
     <h4>Static Method</h4>
     <pre>
     var destination = { foo: { bar: 5, blah: 4 } };
     var source = { foo: { bar: 9 }, test: 'one' };

     // Note, the 'foo' attribute still has the 'blah' property. If we used 'extend' here the entire 'foo'
     // property would be overwritten from the source - which means the 'blah' property would be gone.
     expect($object.merge(destination, source)).toEqual({ foo: { bar: 9, blah: 4 }, test: 'one' });
     </pre>

     <h4>Instance Method</h4>
     <pre>
     var destination = { foo: { bar: 5, blah: 4 } };
     var source = { foo: { bar: 9 }, test: 'one' };
     var obj = $object(destination);

     // Note, the 'foo' attribute still has the 'blah' property. If we used 'extend' here the entire 'foo'
     // property would be overwritten from the source - which means the 'blah' property would be gone.
     expect($object.merge(source)).toEqual({ foo: { bar: 9, blah: 4 }, test: 'one' });
     </pre>
     */
/**
 * @ngdoc service
 * @name transcend.core.$protoFactory
 *
 * @description
 * the '$protoFactory' factory adds prototype methods to classes.
 *
 */
/**
 * @ngdoc service
 * @name transcend.core.$string
 *
 * @description
 * Provides a set of 'string' utility functions. These methods are 'static' functions found on the
 * factory object. In addition, you can call the factory function to retrieve an instantiated 'string'
 * utility instance. This instance has all of the same methods that the static 'factory' object has,
 * except you do not have to pass in the 'string' reference to the function. The point of the instantiable
 * 'string' utility is to cache a particular item and execute multiple calls on that item without having
 * to pass the item reference in each time.
 *
 <pre>
 // Static method call.
 $string.someMethod(stringReference, args);

 // Instantiable method calls. Note, the string reference is not passed in as the first argument anymore.
 var str = $string(stringReference);
 str.someMethod(args);
 str.someOtherMethod(otherArgs);
 </pre>
 *
 * @param {String=} cachedItem The string that you want to 'cache' to perform operations against.
 */
/**
     * @ngdoc method
     * @name transcend.core.$string#format
     * @methodOf transcend.core.$string
     *
     * @description
     * Applies a formatting to a string. The main purpose of this function is to replace {} tokens
     * based on arguments. For example, {0} will be replaced with the 0th indexed argument.
     *
     * @param {String} str The string to apply the formatting on.
     * @param {...String} args Strings to replace tokens in the input string.
     * @returns {String} Formatted string.
     *
     * @example
     <h4>Static Method</h4>
     <pre>
     var formattedString = $string.format('{0} {1}', 'Hello', 'World');
     expect(formattedString).toEqual('Hello World');
     </pre>

     <h4>Instance Method</h4>
     <pre>
     var str = $string('{0} {1} {2}');
     var formattedString = str.format('Hello', 'World');

     // Note, the 'str' instance still has the original '{0} {1} {2}' cached, as its source.
     // More string actions could be applied against that same string source.
     expect(str).toEqual('Hello World');
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.core.$string#prettify
     * @methodOf transcend.core.$string
     *
     * @description
     * Turns text into "readable" text by separating combined text into words and then
     * capitalizing the first character(s).
     *
     * @param {String} text The string make readable/pretty.
     * @returns {String} The readable/pretty string.
     *
     * @example
     <h4>Static Method</h4>
     <pre>
     var text = $string.format('helloWorld');
     expect(text).toEqual('Hello World');
     </pre>

     <h4>Instance Method</h4>
     <pre>
     var str = $string('{0} {1} {2}');
     var text = str.format('helloWorld');

     // Note, the 'str' instance still has the original 'helloWorld' cached, as its source.
     // More string actions could be applied against that same string source.
     expect(text).toEqual('Hello World');
     </pre>
     */
/**
 * @ngdoc service
 * @name transcend.core.directive:searchBox
 *
 * @description
 * The 'searchBox' directive provides a compact search input box.
 *
 * @requires transcend.core#tsConfig
 * @requires $timeout
 *
 * @param {object=} search The search text to bind to.
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">

 <h1>
 <search-box search="ctrl.someText" on-change="ctrl.onChange(value)"></search-box>
 List or people:
 </h1>
 <ul>
 <li ng-repeat="person in ctrl.persons | filter:ctrl.someText">{{person}}</li>
 </ul>
 Searched value: {{ctrl.newValue}}
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.job', 'ngMockE2E'])
 .controller('Ctrl', function() {
     this.someText = '';
     this.persons = ['John Doe', 'Joe Blow', 'Sally Mae', 'Jack Black', 'Billy Joel'];
     this.onChange = function(value){
       this.newValue = value;
     };
  });
 </file>
 </example>
 */
/**
 * @ngdoc filter
 * @name transcend.core.inArray
 *
 * @description
 * Provides the ability to filter an array using another array
 *
 * @requires $filter
 *
 * @param {array} the array which contains the filter values
 * @param {string} the element of the list to match against the array
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl">
 <div ng-repeat='letter in letters | inArray:filterBy:"id"'>{{letter.id}}</div>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.core'])
 .controller('Ctrl', function($scope) {
        $scope.letters = [
          {id: 'a'},
          {id: 'b'},
          {id: 'c'},
          {id: 'd'},
          {id: 'e'}
        ];

      $scope.filterBy = ['b', 'c', 'd'];
      });
 </file>
 </example>
 */
/**
 * @ngdoc filter
 * @name transcend.core.pretty
 *
 * @description
 * Converts a string to an alternative more "readable" format.
 *
 * @requires transcend.core.$string
 * @param {string} text The text to make "pretty".
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl">
 <input ng-model="myText" />
 <p>
 {{myText | pretty}}
 </p>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.core'])
 .controller('Ctrl', function($scope) {
        $scope.myText = 'this is_someSuperUgly_Text';
      });
 </file>
 </example>
 */
